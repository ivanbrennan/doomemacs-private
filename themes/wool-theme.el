;;; wool-theme.el --- inspired by doom-one-theme -*- lexical-binding: t; no-byte-compile: t; -*-

(require 'doom-themes)


;;
;;; Variables

(defgroup wool-theme nil
  "Options for the `wool' theme."
  :group 'doom-themes)

(defcustom wool-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'wool-theme
  :type 'boolean)

(defcustom wool-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'wool-theme
  :type 'boolean)

(defcustom wool-comment-bg nil
  "If non-nil, comments will have a subtle, darker background. Enhancing their
legibility."
  :group 'wool-theme
  :type 'boolean)

(defcustom wool-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line.
Can be an integer to determine the exact padding."
  :group 'wool-theme
  :type '(choice integer boolean))


;;
;;; Theme definition

(def-doom-theme wool
  "A dark theme inspired by moot.vim."

  ;; name        default   256           16
  ;((bg         '("#21242b" "black"       "black"  ))
  ((bg         '("#222327" "black"       "black"  ))
   ;(fg         '("#cbd2df" "#cbd2df"     "brightwhite"  ))
   (fg         '("#cbd2d4" "#cbd2d4"     "brightwhite"  ))

   ;; These are off-color variants of bg/fg, used primarily for `solaire-mode',
   ;; but can also be useful as a basis for subtle highlights (e.g. for hl-line
   ;; or region), especially when paired with the `doom-darken', `doom-lighten',
   ;; and `doom-blend' helper functions.
   ;(bg-alt     '("#1d2026" "black"       "black"        ))
   (bg-alt     '("#202226" "black"       "black"        ))
   (fg-alt     '("#5B6268" "#2d2d2d"     "white"        ))

   ;; These should represent a spectrum from bg to fg, where base0 is a starker
   ;; bg and base8 is a starker fg. For example, if bg is light grey and fg is
   ;; dark grey, base0 should be white and base8 should be black.
   (base0      '("#1B2229" "black"       "black"        ))
   (base1      '("#1c1f24" "#1e1e1e"     "brightblack"  ))
   (base2      '("#202328" "#2e2e2e"     "brightblack"  ))
   (base3      '("#23272e" "#262626"     "brightblack"  ))
   (base4      '("#3f444a" "#3f3f3f"     "brightblack"  ))
   (base5      '("#5B6268" "#525252"     "brightblack"  ))
   (base6      '("#6f82a6" "#6f82a6"     "brightblack"  ))
   (base7      '("#b1bbcf" "#979797"     "brightblack"  ))
   (base8      '("#DFDFDF" "#dfdfdf"     "white"        ))

   (grey       base4)
   (red        '("#ff6c6b" "#ff6655" "red"          ))
   (orange     '("#FCC44C" "#e6c037" "brightred"    ))
   (green      '("#a4d694" "#a4d694" "green"        ))
   (teal       '("#4db5bd" "#44b9b1" "brightgreen"  ))
   (yellow     '("#ECBE7B" "#ECBE7B" "yellow"       ))
   (blue       '("#b3c0e2" "#b3c0e2" "brightblue"   ))
   (dark-blue  '("#2257A0" "#2257A0" "blue"         ))
   (magenta    '("#b8b7e0" "#b8b7e0" "brightmagenta"))
   (violet     '("#a9a1e1" "#a9a1e1" "magenta"      ))
   (cyan       '("#46D9FF" "#46D9FF" "brightcyan"   ))
   (dark-cyan  '("#48778b" "#48778b" "cyan"         ))

   ;; These are the "universal syntax classes" that doom-themes establishes.
   ;; These *must* be included in every doom themes, or your theme will throw an
   ;; error, as they are used in the base theme defined in doom-themes-base.
   (highlight      (doom-lighten blue 0.3))
   (vertical-bar   (doom-darken base1 0.1))
   (selection      (doom-blend blue (doom-blend dark-blue bg-alt 0.08) 0.025))
   (builtin        blue)
   (comments       (if wool-brighter-comments base6 dark-cyan))
   (doc-comments   (doom-lighten comments 0.25))
   (constants      blue)
   (functions      (doom-blend base8 fg 0.5))
   (keywords       blue)
   (methods        cyan)
   (operators      blue)
   (type           yellow)
   (strings        (doom-blend yellow teal 0.8))
   (variables      (doom-blend magenta blue 0.2))
   (numbers        violet)
   (region         (doom-blend violet (doom-blend blue base1 0.1) 0.05))
   (error          red)
   (warning        orange)
   (success        green)
   (vc-modified    orange)
   (vc-added       green)
   (vc-deleted     red)

   ;; These are extra color variables used only in this theme; i.e. they aren't
   ;; mandatory for derived themes.
   (modeline-fg              base7)
   (modeline-fg-alt          base5)

   ;; #1b1d23
   (modeline-bg              (if wool-brighter-modeline
                                 (doom-darken blue 0.475)
                               `(,(doom-darken (car bg) 0.15) ,@(cdr bg))))
   ;; #1b1d23
   (modeline-bg-alt          (if wool-brighter-modeline
                                 (doom-darken blue 0.475)
                               `(,(doom-darken (car bg-alt) 0.15) ,@(cdr bg))))

   ;; #1c1f25
   (modeline-bg-inactive     `(,(doom-darken (car bg) 0.1) ,@(cdr bg)))
   ;; #1c1f25
   (modeline-bg-inactive-alt `(,(doom-darken (car bg-alt) 0.1) ,@(cdr bg)))

   (-modeline-pad
    (when wool-padded-modeline
      (if (integerp wool-padded-modeline) wool-padded-modeline 4))))


  ;;;; Base theme face overrides
  (((line-number &override)
    :foreground (doom-blend dark-blue (doom-darken base5 0.25) 0.08))
   ((line-number-current-line &override)
    :foreground (doom-darken base7 0.1) :background bg) ; :background (doom-lighten bg 0.018)
   ((hl-line &override) :background (doom-lighten bg 0.018))
   ((font-lock-comment-face &override)
    :background (if wool-comment-bg (doom-lighten bg 0.05)) :slant 'italic)
   (font-lock-preprocessor-face :foreground yellow)
   (mode-line
    :background modeline-bg :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg)))
   (mode-line-inactive
    :background modeline-bg-inactive :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))
   (mode-line-emphasis :foreground (if wool-brighter-modeline base8 highlight))
   (mode-line-highlight :foreground base6)
   (lazy-highlight
    :background (doom-lighten bg 0.07) :foreground base8 :distant-foreground base0)
   (isearch :background (doom-lighten bg 0.018) :foreground orange :weight 'ultra-bold)


   ;;;; css-mode <built-in> / scss-mode
   (css-proprietary-property :foreground orange)
   (css-property             :foreground green)
   (css-selector             :foreground blue)
   ;;;; diredfl
   (diredfl-symlink :foreground cyan)
   ;;;; doom-modeline
   (doom-modeline-bar :background (if wool-brighter-modeline modeline-bg highlight))
   (doom-modeline-buffer-file :inherit 'mode-line-buffer-id :weight 'bold)
   (doom-modeline-buffer-path :inherit 'mode-line-emphasis :weight 'bold)
   (doom-modeline-buffer-project-root :foreground green :weight 'bold)
   (doom-modeline-project-dir :foreground dark-cyan :weight 'bold)
   (doom-modeline-buffer-modified :foreground highlight :slant 'italic)
   ;;;; elscreen
   (elscreen-tab-other-screen-face :background "#353a42" :foreground "#1e2022")
   ;;;; evil
   (evil-ex-search :inherit 'isearch)
   ;;;; evil-snipe
   ((evil-snipe-matches-face &override) :underline nil)
   ;;;; haskell-mode
   (haskell-operator-face :foreground operators)
   ;((haskell-pragma-face &override) :foreground (doom-blend yellow base6 0.8))
   (haskell-pragma-face :foreground (doom-darken (doom-blend violet doc-comments 0.0) 0.2))
   ;;;; ivy
   (ivy-current-match :background dark-blue :distant-foreground base0 :weight 'normal)
   ;;;; LaTeX-mode
   (font-latex-math-face :foreground green)
   ;;;; markdown-mode
   (markdown-markup-face :foreground base5)
   (markdown-header-face :inherit 'bold :foreground red)
   ((markdown-code-face &override) :background (doom-lighten base3 0.05))
   ;;;; mic-paren
   (paren-face-match :foreground fg :weight 'ultra-bold)
   ;;;; rainbow-delimiters
   (rainbow-delimiters-depth-1-face :foreground highlight)
   (rainbow-delimiters-depth-2-face :foreground (doom-blend base6 cyan 0.6))
   (rainbow-delimiters-depth-3-face :foreground (doom-lighten base6 0.2))
   (rainbow-delimiters-depth-4-face :foreground (doom-blend blue cyan 0.6))
   (rainbow-delimiters-depth-5-face :foreground highlight)
   (rainbow-delimiters-depth-6-face :foreground (doom-blend base6 cyan 0.6))
   (rainbow-delimiters-depth-7-face :foreground (doom-lighten base6 0.2))
   (rainbow-delimiters-depth-8-face :foreground (doom-blend blue cyan 0.6))
   (rainbow-delimiters-depth-9-face :foreground highlight)
   ;;;; rjsx-mode
   (rjsx-tag :foreground red)
   (rjsx-attr :foreground orange)
   ;;;; solaire-mode
   (solaire-mode-line-face
    :inherit 'mode-line
    :background modeline-bg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-alt)))
   (solaire-mode-line-inactive-face
    :inherit 'mode-line-inactive
    :background modeline-bg-inactive-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-alt))))

  ;;;; Base theme variable overrides-
  ())

;;; wool-theme.el ends here
