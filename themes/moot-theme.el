;;; moot-theme.el --- inspired by moot.vim -*- lexical-binding: t; no-byte-compile: t; -*-

(require 'doom-themes)


;;
;;; Variables

(defgroup moot-theme nil
  "Options for the `moot' theme."
  :group 'doom-themes)

(defcustom moot-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'moot-theme
  :type 'boolean)

(defcustom moot-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'moot-theme
  :type 'boolean)

(defcustom moot-comment-bg moot-brighter-comments
  "If non-nil, comments will have a subtle, darker background. Enhancing their
legibility."
  :group 'moot-theme
  :type 'boolean)

(defcustom moot-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line.
Can be an integer to determine the exact padding."
  :group 'moot-theme
  :type '(choice integer boolean))


;;
;;; Theme definition

(def-doom-theme moot
  "A dark theme inspired by moot.vim."

  ;; name        default   256           16
  ((bg         '("#232323" "black"       "black"  ))
   (fg         '("#cbd2d2" "#cbd2d2"     "brightwhite"  ))

   ;; These are off-color variants of bg/fg, used primarily for `solaire-mode',
   ;; but can also be useful as a basis for subtle highlights (e.g. for hl-line
   ;; or region), especially when paired with the `doom-darken', `doom-lighten',
   ;; and `doom-blend' helper functions.
   (bg-alt     '("#272727" "black"       "black"        ))
   (fg-alt     '("#5B6268" "#2d2d2d"     "white"        ))

   ;; These should represent a spectrum from bg to fg, where base0 is a starker
   ;; bg and base8 is a starker fg. For example, if bg is light grey and fg is
   ;; dark grey, base0 should be white and base8 should be black.
   (base0      '("#1B2229" "black"       "black"        ))
   (base1      '("#1c1f24" "#1e1e1e"     "brightblack"  ))
   (base2      '("#202328" "#2e2e2e"     "brightblack"  ))
   (base3      '("#23272e" "#262626"     "brightblack"  ))
   (base4      '("#3f444a" "#3f3f3f"     "brightblack"  ))
   (base5      '("#5B6268" "#525252"     "brightblack"  ))
   (base6      '("#73797e" "#6b6b6b"     "brightblack"  ))
   (base7      '("#9ca0a4" "#979797"     "brightblack"  ))
   (base8      '("#DFDFDF" "#dfdfdf"     "white"        ))

   (grey       base4)
   (red        '("#ff6c6b" "#ff6655" "red"          ))
   (orange     '("#e6c037" "#e6c037" "brightred"    ))
   (green      '("#a4d694" "#a4d694" "green"        ))
   (teal       '("#4db5bd" "#44b9b1" "brightgreen"  ))
   (yellow     '("#ECBE7B" "#ECBE7B" "yellow"       ))
   (blue       '("#c7d6fc" "#c7d6fc" "brightblue"   ))
   (dark-blue  '("#2257A0" "#2257A0" "blue"         ))
   (magenta    '("#c678dd" "#c678dd" "brightmagenta"))
   (violet     '("#a9a1e1" "#a9a1e1" "magenta"      ))
   (cyan       '("#46D9FF" "#46D9FF" "brightcyan"   ))
   (dark-cyan  '("#48778b" "#48778b" "cyan"         ))

   ;; These are the "universal syntax classes" that doom-themes establishes.
   ;; These *must* be included in every doom themes, or your theme will throw an
   ;; error, as they are used in the base theme defined in doom-themes-base.
   (highlight      (doom-lighten blue 0.25))
   (vertical-bar   (doom-darken base1 0.1))
   (selection      dark-blue)
   (builtin        (doom-blend teal blue 0.4))
   (comments       (if moot-brighter-comments dark-cyan base5))
   (doc-comments   (doom-lighten (if moot-brighter-comments dark-cyan base5) 0.25))
   (constants      (doom-darken (doom-blend violet blue 0.2) 0.1))
   (functions      (doom-blend base8 fg 0.5))
   (keywords       (doom-darken (doom-blend teal blue 0.05) 0.05))
   (methods        cyan)
   (operators      blue)
   (type           yellow)
   (strings        (doom-darken (doom-blend green cyan 0.3) 0.2))
   (variables      (doom-lighten magenta 0.4))
   (numbers        violet)
   (region         (doom-blend blue base1 0.1))
   (error          red)
   (warning        yellow)
   (success        green)
   (vc-modified    orange)
   (vc-added       green)
   (vc-deleted     red)

   ;; These are extra color variables used only in this theme; i.e. they aren't
   ;; mandatory for derived themes.
   (modeline-fg              fg)
   (modeline-fg-alt          base5)
   (modeline-bg              (if moot-brighter-modeline
                                 (doom-darken blue 0.45)
                               (doom-darken bg-alt 0.225)))
   (modeline-bg-alt          (if moot-brighter-modeline
                                 (doom-darken blue 0.475)
                               `(,(doom-darken (car bg-alt) 0.15) ,@(cdr bg))))
   (modeline-bg-inactive     `(,(car bg-alt) ,@(cdr base1)))
   (modeline-bg-inactive-alt `(,(doom-darken (car bg-alt) 0.1) ,@(cdr bg)))

   (-modeline-pad
    (when moot-padded-modeline
      (if (integerp moot-padded-modeline) moot-padded-modeline 4))))


  ;;;; Base theme face overrides
  (((line-number &override) :foreground base4)
   ((line-number-current-line &override) :foreground base7 :background bg)
   ((font-lock-comment-face &override)
    :background (if moot-comment-bg (doom-lighten bg 0.05)) :slant 'italic)
   (mode-line
    :background modeline-bg :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg)))
   (mode-line-inactive
    :background modeline-bg-inactive :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))
   (mode-line-emphasis :foreground (if moot-brighter-modeline base8 highlight))
   (lazy-highlight
    :background (doom-lighten bg 0.1) :foreground base8 :distant-foreground base0)
   (isearch :background (doom-darken highlight 0.1) :foreground base0 :weight 'bold)


   ;;;; css-mode <built-in> / scss-mode
   (css-proprietary-property :foreground orange)
   (css-property             :foreground green)
   (css-selector             :foreground blue)
   ;;;; diredfl
   (diredfl-symlink :foreground cyan)
   ;;;; doom-modeline
   (doom-modeline-bar :background (if moot-brighter-modeline modeline-bg highlight))
   (doom-modeline-buffer-file :inherit 'mode-line-buffer-id :weight 'bold)
   (doom-modeline-buffer-path :inherit 'mode-line-emphasis :weight 'bold)
   (doom-modeline-buffer-project-root :foreground green :weight 'bold)
   (doom-modeline-project-dir :foreground dark-cyan :weight 'bold)
   ;;;; elscreen
   (elscreen-tab-other-screen-face :background "#353a42" :foreground "#1e2022")
   ;;;; evil
   (evil-ex-search :inherit 'isearch)
   ;;;; evil-snipe
   ((evil-snipe-matches-face &override) :underline nil)
   ;;;; haskell-mode
   (haskell-operator-face :foreground operators)
   (haskell-pragma-face :foreground (doom-darken (doom-blend violet doc-comments 0.3) 0.2))
   ;;;; ivy
   (ivy-current-match :background dark-blue :distant-foreground base0 :weight 'normal)
   ;;;; LaTeX-mode
   (font-latex-math-face :foreground green)
   ;;;; markdown-mode
   (markdown-markup-face :foreground base5)
   (markdown-header-face :inherit 'bold :foreground red)
   ((markdown-code-face &override) :background (doom-lighten base3 0.05))
   ;;;; rainbow-delimiters
   (rainbow-delimiters-depth-1-face :foreground blue)
   ;(rainbow-delimiters-depth-2-face :foreground yellow)
   (rainbow-delimiters-depth-2-face :foreground (doom-blend base6 cyan 0.6))
   (rainbow-delimiters-depth-3-face :foreground base7)
   ;(rainbow-delimiters-depth-4-face :foreground (doom-lighten teal 0.2))
   ;(rainbow-delimiters-depth-4-face :foreground (doom-darken yellow 0.1))
   (rainbow-delimiters-depth-4-face :foreground (doom-darken green 0.1))
   (rainbow-delimiters-depth-5-face :foreground blue)
   (rainbow-delimiters-depth-6-face :foreground (doom-blend base6 cyan 0.6))
   (rainbow-delimiters-depth-7-face :foreground base7)
   (rainbow-delimiters-depth-8-face :foreground orange)
   (rainbow-delimiters-depth-9-face :foreground violet)
   ;;;; rjsx-mode
   (rjsx-tag :foreground red)
   (rjsx-attr :foreground orange)
   ;;;; solaire-mode
   (solaire-mode-line-face
    :inherit 'mode-line
    :background modeline-bg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-alt)))
   (solaire-mode-line-inactive-face
    :inherit 'mode-line-inactive
    :background modeline-bg-inactive-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-alt))))

  ;;;; Base theme variable overrides-
  ())

;;; moot-theme.el ends here
