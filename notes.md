# notes

Turn off orderless?

## Learn

- https://github.com/noctuid/evil-guide#mapping-under-keys-that-arent-prefix-keys
  - https://github.com/noctuid/general.el#mapping-under-non-prefix-keys
- https://github.com/noctuid/evil-guide
- https://github.com/tecosaur/emacs-everywhere/issues/49
- https://discourse.doomemacs.org/t/breaking-change-on-b9933e663771/2957
- https://github.com/hlissner/.doom.d/blob/master/lisp/modeline.el
- https://discourse.doomemacs.org/t/how-to-re-bind-keys/56
- https://github.com/hlissner/doom-emacs-private
- https://discourse.doomemacs.org/t/keeping-font-size-everywhere/2799
- https://discourse.doomemacs.org/t/common-config-anti-patterns/119
- https://discourse.doomemacs.org/t/how-to-switch-customize-or-write-themes/37
- https://emacs.stackexchange.com/a/24913/8528
- https://github.com/doomemacs/doomemacs/issues/1307
- https://unix.stackexchange.com/a/81851/47044
- https://github.com/direnv/direnv/wiki/PS1
- https://emacs.stackexchange.com/questions/60168/how-can-i-set-wm-class-for-emacs-27
- https://github.com/nix-community/nix-direnv
- https://magit.vc/manual/transient/index.html
- https://github.com/hlissner/.doom.d/blob/2529bcd46c5aad0cbe796793e5c08e1faf5ba272/config.el#L101

## TODO

## Questions

Wouldn't it be great if vertico-cycle didn't wrap around when you reach the end of the list due to key-repeat, but only if you intentionally tap down after already having reached the bottom of the list?

How to control scroll on window splits?

How to migrate ~/.emacs.d to ~/.config/emacs?
> And as of Emacs 27, =~/.emacs.d= can be moved to =~/.config/emacs=.

How to sign commits in Magit?
https://www.emacswiki.org/emacs/Comments_on_Magit
https://github.com/tecosaur/emacs-config/blob/master/config.org#personal-information

How to ignore case when performing completion?

How to fix forward completion edge-case?
https://emacs.stackexchange.com/questions/72875/unexpected-dabbrev-expand-forward-completion-behavior

How to bind "C-."?
https://github.com/noctuid/evil-guide#mapping-under-keys-that-arent-prefix-keys

How to reproduce Vim's C-x C-n / C-x C-l bindings?

How to make Ctrl-v paste when in insert state?

Do I need to run `doom sync` after each `nixos-rebuild`?

How to use Ctrl-. and Ctrl-, for M-. and M-,

```sh
mkdir ~/.config/doom/themes
wget -O ~/.config/doom/themes/moot-theme.el https://raw.githubusercontent.com/doomemacs/themes/d79a41f593c69697af1ddaac971c0c47ecc446a8/themes/doom-one-theme.el
# replace occurrences of "doom-one" with "moot"
```
```elisp
(setq doom-theme 'moot)
```

## Misc

stop the emacs daemon
```sh
emacsclient -e '(kill-emacs)'
```

start emacs with no init file, no site file, no spash, and without processing X resources
```sh
emacs -Q
```

doom-big-font-mode: `SPC t b`

get face under cursor: `SPC u g a`

`describe-prefix-bindings`

To open a file as root, `SPC f u`. Auth source will offer to save the login credentials to ~/.emacs.d/.local/etc/authinfo.gpg
Magit seems to work as expected when using this approach to edit nixos configuration files.
