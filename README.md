# doom-emacs-private

Private config for [doom-emacs](https://github.com/hlissner/doom-emacs).

## Install

```sh
# private config
git clone git@codeberg.org:ivanbrennan/doom-emacs-private.git ~/.config/doom

# doom
git clone https://github.com/doomemacs/doomemacs ~/.emacs.d
~/.emacs.d/bin/doom install
~/.emacs.d/bin/doom doctor
```

## References

- [Doom Emacs](https://github.com/doomemacs/doomemacs)
- [Discourse FAQ](https://discourse.doomemacs.org/faq)
- [More Discourse FAQ](https://discourse.doomemacs.org/tag/faq)
- [Documentation](https://github.com/doomemacs/doomemacs/blob/master/docs/index.org)
- [Example Configurations](https://discourse.doomemacs.org/t/example-user-configurations/38)
