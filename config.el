;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Ivan Brennan"
      user-mail-address "ivan.brennan@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
(setq doom-font (font-spec :family "Source Code Pro" :size 14.0)
      doom-variable-pitch-font (font-spec :family "Noto Sans" :size 18.0))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'wool)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")


(setq scroll-margin 1)

;; Don't generate a doom env file in $EMACSDIR/.local/env because:
;; 1. it doesn't seem to be necessary on my system
;; 2. it breaks the ability to inherit the env from a nix-shell
;;
;; In fact, it seems likely the best approach on my system to not generate a
;; doom env file at all. However, let's start with a less drastic approach,
;; generating it in a non-standard location that gives me more control over
;; whether or not to load it:
;;
;;   doom sync && doom env -o ~/.config/doom/env
;;
;; The new profile system appears to be another approach I could use, but I
;; may wait for that to stabilize a bit before trying it.
(when (and (or (display-graphic-p)
               (daemonp))
           (not (getenv-internal "IN_NIX_SHELL")))
  (doom-load-envvars-file (expand-file-name "env" doom-private-dir) 'noerror))

;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
(after! doom-themes
  (setq moot-brighter-comments t
        moot-comment-bg nil
        wool-brighter-comments t))

(defun ivan-force-reload-theme ()
  (interactive)
  (let ((theme (car custom-enabled-themes)))
    (when theme
      (load-theme theme t))))

(after! doom-modeline
  ;; An evil mode indicator is redundant with cursor shape
  (advice-add #'doom-modeline-segment--modals :override #'ignore))

(after! company
  ;; Don't show completions unless we ask for it.
  (setq company-idle-delay nil))

(setq! evil-want-C-u-scroll nil
       evil-want-C-d-scroll nil
       evil-want-C-w-in-emacs-state t)

(after! evil
  (setq evil-echo-state nil
        evil-visual-state-cursor 'box)
  (define-key! :keymaps +default-minibuffer-maps
    "C-r" #'isearch-backward
    "M-r" #'evil-paste-from-register
    "M-R" #'previous-matching-history-element
    "M-v" #'clipboard-yank)
  (map! :after winner
        (:map evil-window-map
         "U" #'winner-redo))
  (map! :map evil-window-map
        "q" #'evil-save-modified-and-close))

(map! :after winner
      :leader
      "w U" #'winner-redo)

(setq-hook! 'ruby-mode-hook
  tab-width 2)

;; NOTE: Occasionally, `evil-visual-char' and `evil-visual-line' stop working in
;; some buffers. Manually evaluating `(setq-local transient-mark-mode t)' seems
;; to fix it.
;; https://github.com/emacs-evil/evil/issues/1122#issuecomment-740269730

(map! (:after evil
       :n "U" #'evil-redo
       :n "C-r" nil
       :n "S-<return>" #'ivan-add-whitespace-above
       :n "M-<return>" #'ivan-add-whitespace-below
       :n "C-<return>" #'ivan-add-whitespace-below
       ;; TODO: Make this kill the buffer if this is the last window/workspace
       :m "C-d" #'+workspace/close-window-or-workspace
       :m "C-k" #'evil-scroll-line-up
       :m "C-j" #'evil-scroll-line-down
       :m "C-e" #'evil-end-of-line
       :v "<backspace>" #'delete-active-region
       :i "C-<return>" #'evil-open-below
       :i "S-<return>" #'evil-open-above
       :i "C-e" #'evil-copy-from-below
       :i "C-d" nil
       :i "C-t" nil
       :ie "M-v" #'clipboard-yank))

(after! isearch
  (defun isearch-done-other-end (&optional nopush edit)
    "End current search at the other end of the match."
    (interactive)
    (funcall #'isearch-done nopush edit)
    (when isearch-other-end
      (goto-char isearch-other-end)))
  (map! :map isearch-mode-map
        "<return>"   #'isearch-done-other-end
        "C-<return>" #'isearch-done))

;; TODO: :after evil
(map! :leader
      :desc "Switch to last buffer" "l" #'evil-switch-to-windows-last-buffer
      :desc "Pop up scratch buffer" "`" #'doom/open-scratch-buffer
      :desc "M-x"                   "x" #'execute-extended-command)
(map! :map evil-ex-search-keymap
      "M-v" #'clipboard-yank)
(map! :map evil-ex-completion-map
      "M-v" #'clipboard-yank)
(map! :map minibuffer-local-isearch-map
      "M-v" #'clipboard-yank)
(map! :map isearch-mode-map
      "M-v" #'isearch-yank-kill)

(map! (:map doom-leader-workspace-map
       :desc "Rename workspace" "m" #'+workspace/rename
       "r" nil)
      :leader
      :desc "Switch to last workspace" "L" #'+workspace/other
      :desc "Switch workspace" ">" #'+workspace/switch-to)
;; NOTE: Calling `:desc "Rename workspace"' above doesn't currently work.
;; https://github.com/doomemacs/doomemacs/issues/5532#issuecomment-991611197
(after! persp-mode
  (which-key-add-keymap-based-replacements
    doom-leader-workspace-map
    "m"
    "Rename workspace"))

(map! :map doom-leader-toggle-map
      :desc "Highlight current line"
      "h" #'hl-line-mode)

(add-hook! (prog-mode org-mode)
  (defun underscore-is-word-char () (modify-syntax-entry ?_ "w")))

(add-hook! (clojure-mode emacs-lisp-mode)
  (defun hyphen-is-word-char () (modify-syntax-entry ?- "w")))

(add-hook! (helpful-mode org-mode doom-docs-mode) #'mixed-pitch-mode)

(after! emacs-everywhere
  ;; https://github.com/tecosaur/emacs-everywhere/issues/49
  (setq emacs-everywhere-mode-initial-map nil)

  ;; Easier to match with an XMonad rule.
  (setq emacs-everywhere-frame-name-format "emacs-anywhere")

  ;; The emacs-everywhere-init-hooks run before emacs-everywhere-mode is enabled,
  ;; but hiding the modeline only seems to work if run afterwards.
  (defadvice! hide-emacs-everywhere-modeline (&optional _)
    "Hide the modeline after emacs-everywhere-mode is enabled."
    :after #'emacs-everywhere-mode
    (when emacs-everywhere-mode
      (hide-mode-line-mode)))

  ;; Let XMonad do its job.
  (advice-add #'emacs-everywhere-set-frame-position :override #'ignore))

;;; :ui doom-dashboard
;; https://gitlab.com/zzamboni/dot-doom/-/tree/master/splash
(setq fancy-splash-image (concat doom-private-dir "doom-emacs-bw-light.svg"))
(remove-hook '+doom-dashboard-functions #'doom-dashboard-widget-shortmenu)
(remove-hook '+doom-dashboard-functions #'doom-dashboard-widget-loaded)
(remove-hook '+doom-dashboard-functions #'doom-dashboard-widget-footer)

(setq magit-repository-directories '(("~/Development" . 1))
      ;; This is not a Google Doc.
      magit-save-repository-buffers nil
      ;; Don't restore the wconf after quitting magit.
      magit-inhibit-save-previous-winconf t)

(defun ivan-add-whitespace-above ()
  (interactive)
  (save-excursion
    (beginning-of-line)
    (newline)))

(defun ivan-add-whitespace-below ()
  (interactive)
  (save-excursion
    (end-of-line)
    (newline)))

(defun ivan-get-faces (pos)
  "Get the font faces at POS."
  (-distinct
   (remq nil
         (list
          (get-char-property pos 'read-face-name)
          (get-char-property pos 'face)
          (plist-get (text-properties-at pos) 'face)))))

(defun ivan-display-faces (pos)
  "Display the font faces at POS."
  (interactive "d")
  (princ (ivan-get-faces pos)))

(map! :map doom-leader-map
      :desc "Show faces at point"
      "\\" #'ivan-display-faces)

(after! hl-line
  ;; Highlight current line only in the selected window.
  (setq hl-line-sticky-flag nil))

(add-hook! magit-mode
  (hl-line-mode -1))

;; NOTE: `blink-matching-paren' has no effect when `show-paren-mode' is enabled.
;; It's not very satisfying with a bar-shaped cursor anyway, so no great loss in
;; `evil-insert-state'. It might be nice, however, to use it in the minibuffer.
;(after! simple
;  (setq blink-matching-paren 'jump
;        blink-matching-delay 0.35))

(defun ivan-fixup-dabbrev--search (abbrev reverse ignore-case)
  (save-match-data
    (let ((orig-point (point))
          (pattern1 (concat (regexp-quote abbrev)
                            "\\(" dabbrev--abbrev-char-regexp "\\)"))
          (pattern2 (concat (regexp-quote abbrev)
                            "\\(\\(" dabbrev--abbrev-char-regexp "\\)+\\)"))
          ;; This makes it possible to find matches in minibuffer prompts
          ;; even when they are "inviolable".
          (inhibit-point-motion-hooks t)
          found-string result)
      ;; Limited search.
      (save-restriction
        (and dabbrev-limit
             (narrow-to-region
              dabbrev--last-expansion-location
              (+ (point) (if reverse (- dabbrev-limit) dabbrev-limit))))
        ;;--------------------------------
        ;; Look for a distinct expansion, using dabbrev--last-table.
        ;;--------------------------------
        (while (and (not found-string)
                    (if reverse
                        (re-search-backward pattern1 nil t)
                      (re-search-forward pattern1 nil t)))
          (goto-char (match-beginning 0))
          ;; In case we matched in the middle of a word,
          ;; back up to start of word and verify we still match.
          (dabbrev--goto-start-of-abbrev)
          ;; NOTE: This is where we deviate from the original function.
          (unless reverse
            (goto-char (max orig-point (point))))

          (if (not (looking-at pattern1))
              nil
            ;; We have a truly valid match.  Find the end.
            (re-search-forward pattern2)
            (setq found-string (match-string-no-properties 0))
            (setq result found-string)
            (and ignore-case (setq found-string (downcase found-string)))
            ;; Ignore this match if it's already in the table.
            (if (dabbrev-filter-elements
                 table-string dabbrev--last-table
                 (string= found-string table-string))
                (setq found-string nil)))
          ;; Prepare to continue searching.
          (goto-char (if reverse (match-beginning 0) (match-end 0))))
        ;; If we found something, use it.
        (when found-string
          ;; Put it into `dabbrev--last-table'
          ;; and return it (either downcased, or as is).
          (setq dabbrev--last-table
                (cons found-string dabbrev--last-table))
          result)))))

(after! dabbrev
  ;; Temporary fix for bug described at https://emacs.stackexchange.com/questions/72875/unexpected-dabbrev-expand-forward-completion-behavior
  (advice-add #'dabbrev--search :override #'ivan-fixup-dabbrev--search))

(add-to-list 'auto-mode-alist '("/bashrc\\'" . sh-mode))

;(remove-hook 'doom-first-input-hook #'evil-snipe-mode)
